void main() {
  int counter = 0;
  while (counter < 2) {
    print(counter);
    counter++;
  }

  do {
    print(counter);
    counter++;
  } while (counter < 2);
}
